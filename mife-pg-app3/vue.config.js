module.exports = {
  chainWebpack: (config) => {
    config.devServer.headers({
      'Access-Control-Allow-Origin': '*',
    });
    config.devServer.set('disableHostCheck', false);
    config.devServer.set('sockPort', 8083);
    config.devServer.set('sockHost', 'localhost');
    config.devServer.set('port', 8083);
    config.devServer.set('inline', false);
    config.devServer.set('hot', true);
  },
};
