import { registerApplication, start } from "single-spa";
import {
  constructApplications,
  constructRoutes,
  constructLayoutEngine,
} from "single-spa-layout";
import microfrontendLayout from "./microfrontend-layout.html";
import "./styles.css"
import mitt from "mitt";

window.PubSub = mitt();
window.PubSub.on('layout-change', (event) => {
  console.log('>>> Root config received "layout-change" event:', event);

  console.log('Changing to layout ', event.data.layoutType)

  switch (event?.data?.layoutType) {
    case 1:
      document.querySelector('.shell_nav--left').classList.add('shell_nav--collapse');
      break;
    case 2:
      document.querySelector('.shell_nav--left').classList.add('shell_nav--collapse');
      document.querySelector('.shell_nav--top').classList.add('shell_nav--collapse');
      break;
    default:
      document.querySelector('.shell_nav--left').classList.remove('shell_nav--collapse');
      document.querySelector('.shell_nav--top').classList.remove('shell_nav--collapse');
      break;
  }
});

const routes = constructRoutes(microfrontendLayout);
const applications = constructApplications({
  routes,
  loadApp({ name }) {
    return System.import(name);
  },
});
const layoutEngine = constructLayoutEngine({ routes, applications });


applications.forEach(registerApplication);
layoutEngine.activate();
start();